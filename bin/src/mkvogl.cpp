/**************************************************************************
 *
 * Copyright 2013-2014 RAD Game Tools and Valve Software
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <argp.h> 
#include <sys/utsname.h>
#include <string>

#define F_VERBOSE         0x00000001
#define F_I386            0x00000002
#define F_AMD64           0x00000004
#define F_USEMAKE         0x00000008
#define F_CLEAN           0x00000010
#define F_RELEASE         0x00000020
#define F_DEBUG           0x00000040
#define F_SCANBUILD       0x00000100
#define F_CLANG34         0x00000200
#define F_GCC48           0x00000400
#define F_GCC             0x00000800
#define F_CLEANONLY       0x00001000
#define F_IGNOREERRORS    0x00002000


#define EXIT_ON_FAIL(_syscall) \
    do { \
        int _syscallStatus = _syscall;
        

static bool g_dryrun = false;

struct arguments_t
{
    unsigned int flags;
    std::string defs;
};

void exit_on_fail(const arguments_t& _flags, int _retcode)
{
    const bool verbose = _flags.flags & F_VERBOSE;
    const bool retcode_failure = _retcode != 0;
    const bool ignore_errors = _flags.flags & F_IGNOREERRORS;

    if (retcode_failure)
    {
        printf("ERROR: System Call failed (return code: %d), see above for info.\n", _retcode);
        if (!ignore_errors)
            exit(_retcode);
        printf("WARNING: Attempting to continue because '--ignore-errors/-i' was specified.\n");
        return;
    } 

    if (verbose)
        printf("System call return code: %d\n", _retcode);
}

//----------------------------------------------------------------------------------------------------------------------
// Printf style formatting for std::string.
//----------------------------------------------------------------------------------------------------------------------
std::string string_format(const char *fmt, ...)
{
    std::string str;
    int size = 256;

    for (;;)
    {
        va_list ap;

        va_start(ap, fmt);
        str.resize(size);
        int n = vsnprintf((char *)str.c_str(), size, fmt, ap);
        va_end(ap);

        if ((n > -1) && (n < size))
        {
            str.resize(n);
            return str;
        }

        size = (n > -1) ? (n + 1) : (size * 2);
    }

    return str;
}

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    arguments_t *arguments = (arguments_t *)state->input;

    switch (key)
    {
    case '?': 
        argp_state_help(state, stderr, ARGP_HELP_LONG | ARGP_HELP_EXIT_OK);
        break;
    case -1:
        {
            const char *argval = &state->argv[state->next - 1][2];
            for (size_t i = 0; ; i++)
            {
                const char *name = state->root_argp->options[i].name;
                if (!name)
                    break;
                if (!strcmp(name, argval))
                {
                    arguments->defs += " -D";
                    arguments->defs += &state->argv[state->next - 1][2];
                    arguments->defs += "=On";
                    return 0;
                }
            }
            printf("Unrecognized option: '%s'\n\n", argval);
            argp_state_help(state, stderr, ARGP_HELP_LONG | ARGP_HELP_EXIT_OK);
        }
        break;
    case 'v': arguments->flags |= F_VERBOSE; break;
    case 'i': arguments->flags |= F_IGNOREERRORS; break;
    case '3': arguments->flags |= F_I386; break;
    case '6': arguments->flags |= F_AMD64; break;
    case 'm': arguments->flags |= F_USEMAKE; break;
    case 'c': arguments->flags |= F_CLEAN; break;
    case 'o': arguments->flags |= F_CLEANONLY; break;
    case 'd': arguments->flags |= F_DEBUG; break;
    case 'r': arguments->flags |= F_RELEASE; break;
    case 'g': arguments->flags |= F_GCC; break;
    case '8': arguments->flags |= F_GCC48; break;
    case 'n': arguments->flags |= F_CLANG34; break;
    case 's': arguments->flags |= F_SCANBUILD | F_USEMAKE | F_CLANG34 | F_CLEAN; break;
    case 'y': g_dryrun = true; break;
        break;
    }
    return 0; 
}

void errorf(const char *format, ...)
{
    va_list args;

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

    exit(-1);
}

int systemf(const char *format, ...)
{
    char command[PATH_MAX];
    int retval = 0;

    va_list args;
    va_start(args, format);
    vsnprintf(command, sizeof(command), format, args);

    printf("  %s\n", command);
    if (!g_dryrun)
        retval = system(command); 

    va_end(args);

    return retval;
}

int main(int argc, char *argv[])
{
    static struct argp_option options[] =
    {
        { "i386",                       '3', 0, 0, "Build 32-bit", 0 },
        { "amd64",                      '6', 0, 0, "Build 64-bit.", 0 },

        { "release",                    'r', 0, 0, "Build release (default).", 1 },
        { "debug",                      'd', 0, 0, "Build debug.", 1 },

        { "clean",                      'c', 0, 0, "Clean (remove directory before build).", 2 },
        { "cleanonly",                  'o', 0, 0, "Clean only (remove directory and exit).", 2 },
        { "usemake",                    'm', 0, 0, "Use make (not Ninja).", 2 },
        { "dry-run",                    'y', 0, 0, "Don't execute commands.", 2 },
        { "verbose",                    'v', 0, 0, "Produce verbose output", 2 },
        { "ignore-errors",              'i', 0, 0, "Continue even if subcommands fail", 2 },

        { "gcc46",                      'g', 0, 0, "Use gcc 4.6 to build.", 3 },
        { "gcc48",                      '8', 0, 0, "Use gcc 4.8 to build.", 3 },
        { "clang34",                    'n', 0, 0, "Use clang 3.4 to build.", 3 },
        { "scan-build",                 's', 0, 0, "Use Clang Static Analyzer (scan-build) to build.", 3 },

        { "HAS_UPDATED_LIBUNWIND",      -1,  0, 0, "Build libbacktrace with HAS_UPDATED_LIBUNWIND.", 100 },
        { "VOGLTEST_LOAD_LIBVOGLTRACE", -1,  0, 0, "Don't implicitly link against libgl.so", 100 },
        { "VOGLTRACE_NO_PUBLIC_EXPORTS", -1,  0, 0, "Don't define public GL exports in libvogltrace.so.", 100 },
        { "VOGL_ENABLE_ASSERTS",        -1,  0, 0, "Enable assertions in voglcore builds (including release).", 100 },
        { "USE_TELEMETRY",              -1,  0, 0, "Build with Telemetry.", 100 },
        { "WITH_ASAN",                  -1,  0, 0, "Build with Clang address sanitizer.", 100 },
        { "USE_MALLOC",                 -1,  0, 0, "Use system malloc (not STB Malloc).", 100 },
        { "CMAKE_VERBOSE",              -1,  0, 0, "Do verbose cmake.", 100 },

        { "help",                       '?', 0, 0, "Give this help message.", -1 },
        { 0 }
    };

    const char *vogl_proj_dir = getenv("VOGL_PROJ_DIR");
    if (!vogl_proj_dir)
    {
        printf("ERROR: VOGL_PROJ_DIR envvar not set.\n");
        return -1;
    }

    arguments_t args;
    args.flags = 0;
    struct argp argp = { options, parse_opt, 0, "vogl project builder." };

    // Argp moves our argvs around (jerks) - save original order here so we
    //  can launch cmake directly down below if needed.
    std::string cmdline_orig;
    for(int i = 1; i < argc; i++)
    {
        bool quote_arg = !!strchr(argv[i], ' ');
        if (i > 1)
            cmdline_orig += " ";
        if (quote_arg)
            cmdline_orig += "\"";
        cmdline_orig += argv[i];
        if (quote_arg)
            cmdline_orig += "\"";
    }

    // Parse args.
    argp_parse(&argp, argc, argv, ARGP_NO_HELP, 0, &args);

    // Check if we are running under a chroot.
    const char *schroot_user = getenv("SCHROOT_USER");
    if (schroot_user)
    {
        struct utsname uts;

        if(uname(&uts))
            errorf("ERROR: uname failed.\n");

        unsigned int plat_flag = !strcmp(uts.machine, "x86_64") ? F_AMD64 : F_I386;
        args.flags |= plat_flag;

        if ((args.flags & (F_AMD64 | F_I386)) != plat_flag)
            errorf("ERROR: Can't build this arch in this %s chroot.\n", uts.machine);
    }

    if (!(args.flags & (F_I386 | F_AMD64)))
        errorf("ERROR: Need to specify --i386 or --amd64\n");

    // Default to release if not set.
    if (!(args.flags & (F_RELEASE | F_DEBUG)))
        args.flags |= F_RELEASE;

    // Use Ninja if set.
    if (!(args.flags & F_USEMAKE))
        args.defs += " -G Ninja";

    // Get the vogl src directory and add it to our defines.
    args.defs += " ";
    args.defs += vogl_proj_dir;

    unsigned int platform = args.flags & (F_I386 | F_AMD64);
    while (platform)
    {
        static const char *libarch[] = { "i386", "x86_64" };
        static const char *suffix[] = { "32", "64" };
        static const char *schroot_name[] = { "vogl_precise_i386", "vogl_precise_amd64" };
        static const char *deflibarch[] = { " -DCMAKE_LIBRARY_ARCHITECTURE=i386-linux-gnu", " -DCMAKE_LIBRARY_ARCHITECTURE=x86_64-linux-gnu" };
        int platind = !(platform & F_I386);

        platform &= (platform - 1);

        unsigned int flavor = args.flags & (F_RELEASE | F_DEBUG);
        while (flavor)
        {
            static const char *name[] = { "Release32", "Debug32", "Release64", "Debug64" };
            static const char *build_type[] = { " -DCMAKE_BUILD_TYPE=Release", " -DCMAKE_BUILD_TYPE=Debug" };
            int flavind = !(flavor & F_RELEASE);

            flavor &= (flavor - 1);

            const char *flavor_str = name[2*platind + flavind];
            printf("\033[0;93mBuilding %s - %s\033[0m\n", libarch[platind], flavor_str);

            std::string defines = args.defs;
            defines += build_type[flavind];

            // We need to add this for Ninja builds in the chroots. (Bug in cmake+Ninja)
            if (!(args.flags & F_USEMAKE))
                defines += deflibarch[platind];

            // Add path to Qt5Config.cmake
            defines += string_format(" -DQt5_DIR=%s/vogl_extbuild/%s/qt-everywhere-opensource-src-5.3.0_rev1/qtbase/lib/cmake/Qt5", vogl_proj_dir, libarch[platind]).c_str();

            printf("\033[0;93m %s\033[0m\n", defines.c_str());

            // Get our build directory.
            std::string build_dir = string_format("%s/vogl_build/%s", vogl_proj_dir, flavor_str);

            // If we are building clean, remove all the files in our build directory.
            if (args.flags & (F_CLEAN | F_CLEANONLY) )
            {

                static const char *subdirs[] = { ".", "OGLSamples_Truc", "OGLSuperBible" };
                for (size_t i = 0; i < sizeof(subdirs) / sizeof(subdirs[0]); i++)
                {
                    // Don't exit on fail for this or other clean-related things.
                    systemf("exec rm -f %s/vogl_build/%s/*%s 2> /dev/null", vogl_proj_dir, subdirs[i], suffix[platind]);
                    systemf("exec rm -f %s/vogl_build/%s/*%s.so 2> /dev/null", vogl_proj_dir, subdirs[i], suffix[platind]);
                }

                // Don't exit on fail for this or other clean-related things.
                systemf("exec rm -rf %s/vogl_build/%s/*", vogl_proj_dir, flavor_str);

                if (args.flags & F_CLEANONLY)
                    return 0;
            }

            // Make sure our build dir exists.
            exit_on_fail(args, systemf("exec mkdir -p %s", build_dir.c_str()));

            if (!g_dryrun)
            {
                // Cd to build directory.
                int ret = chdir(build_dir.c_str());
                if (ret != 0)
                    errorf("ERROR: chdir(%s) failed: %d\n", build_dir.c_str(), ret);
            }

            // Check if we are in a schroot. If so, get commands to switch to chroot before executing makes.
            std::string schroot_prefix;
            if (!schroot_user)
            {
                // Will be something like: schroot -c prefix_amd64 -d /build/dir/is/here/bin64/Debug -- 
                schroot_prefix = string_format("schroot -c %s -d \"%s\" -- ", schroot_name[platind], build_dir.c_str());
            }

            std::string set_compiler = string_format("%s/bin/chroot_set_compiler.sh", vogl_proj_dir);

            if (args.flags & F_GCC48)
                set_compiler += " -8";
            else if (args.flags & F_GCC)
                set_compiler += " -g";
            else if (args.flags & F_CLANG34)
                set_compiler += " -c";
            else
                set_compiler += " -q";

            std::string scan_build = (args.flags & F_SCANBUILD) ? "scan-build " : "";
            exit_on_fail(args, systemf("exec %s%s", schroot_prefix.c_str(), set_compiler.c_str()));

            // Always do the cmake command. Seems fast, and it rebuilds stuff if its changed.
            exit_on_fail(args, systemf("exec %s%scmake %s", schroot_prefix.c_str(), scan_build.c_str(), defines.c_str()));


            // Check if we are using Ninja or regular makefiles.
            std::string buildfile = build_dir + "/build.ninja";
            bool use_ninja = (access(buildfile.c_str(), F_OK) == 0);
            static const char *make_command = use_ninja ? "ninja" : "make -j 8";
            static const char *verbose_arg = use_ninja ? " -v" : " VERBOSE=1";

            exit_on_fail(args, systemf("exec %s%s%s%s", schroot_prefix.c_str(), scan_build.c_str(), make_command, (args.flags & F_VERBOSE) ? verbose_arg : ""));
        }
    }

    return 0;
}
